var socket = io(),
	div    = document.getElementsByClassName("messages")[0],
	name   = getQueryVariable('name'),
	room   = getQueryVariable('room');
	

socket.on('connect', function() {
	console.log("Connected via socket.io");
});

socket.on('message', function(message) {
	var timestamp       = moment().valueOf(),
		timeStampMoment = moment.utc(timestamp);

	console.log("New Message");
	console.log(message.name);

	var p   = document.createElement('p'),
		par = document.createElement('p');

	p.innerHTML = (message.name + " wants to join room " + room + " " + "<strong>" + timeStampMoment.format('h:mma') + ": </strong>" );	
    
   par.innerHTML = (message.text);
    div.appendChild(par);
	div.appendChild(p);
	


});

//handles submitting of new message

var form  = document.getElementById("message-form"),
	input = form.querySelector('input[name=message]');
   

form.addEventListener("submit", function(e) {
	e.preventDefault();
	socket.emit('message', {
		name: name,
		text: input.value
	});

	//clear text

	input.value = "";
});