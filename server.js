var PORT     = process.env.PORT || 3000,
    express  = require("express"),
    app      = express(),
    http     = require("http").Server(app),
    io		 = require("socket.io")(http),
    moment   = require("moment");

app.use(express.static(__dirname + '/public'));

io.on('connection', function(socket) {
	console.log("User connected via socket.io");

	socket.on("message", function(message) {
		console.log('Message Received:' + message.text);

	message.timestamp = moment().valueOf()

		io.emit("message", message)
	});

	
//to also send to the sender io.emit()
	socket.emit('message', {
		name: "system",
		timestamp: moment().valueOf(),
		text: 'welcome to the chat application'

		
	});
});

http.listen(PORT, function() {
	console.log("Server started ...")
});